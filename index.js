const { readFileSync } = require('fs');
const path = require('path');
const Text = require('markov-chains-text').default;

const dan = readFileSync(path.join(__dirname, 'data/dan.txt'), 'utf-8');
const gen = new Text(dan, { stateSize: 3 });
 
// generate a sentence
let fails = 0;
let total = 1000;
for (let i = 0; i < total; i++) {
  const sentence = gen.makeSentence({ tries: 100 });
  if (/^Error/.test(sentence)) fails++;
}
 
console.log(fails, total);